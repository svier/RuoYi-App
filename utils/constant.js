const constant = {
   avatar: 'vuex_avatar',
   name: 'vuex_name',
   roles: 'vuex_roles',
   permissions: 'vuex_permissions',
   workbench: 'vuex_workbench'
 }

 export default constant
