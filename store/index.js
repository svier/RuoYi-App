import Vue from 'vue'
import Vuex from 'vuex'
import user from '@/store/modules/user'
import workbench from '@/store/modules/workbench'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
	workbench
  },
  getters
})

export default store
